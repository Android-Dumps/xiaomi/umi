## qssi-user 12
12 SKQ1.211006.001 V13.0.4.0.SJBCNXM release-keys
- Manufacturer: xiaomi
- Platform: kona
- Codename: umi
- Brand: Xiaomi
- Flavor: qssi-user
- Release Version: 12
12
- Id: SKQ1.211006.001
- Incremental: V13.0.4.0.SJBCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Xiaomi/umi/umi:12/RKQ1.211001.001/V13.0.4.0.SJBCNXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.211006.001-V13.0.4.0.SJBCNXM-release-keys
- Repo: xiaomi/umi
